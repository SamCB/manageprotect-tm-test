import sys

sys.path.append('/usr/exchangelib/')

from exchangelib import DELEGATE
from exchangelib.account import Account
from exchangelib.configuration import Configuration
from exchangelib.subscription import Subscription

import os

import logging
import time

#logging.basicConfig(level="DEBUG")
log = logging.getLogger(__name__)

def print_event(event, item):
    print("==================================================================")
    print(event)
    if item:
        #import pdb; pdb.set_trace()
        print("SENDER:", item.sender)
        print("RECIPIENTS:", item.to_recipients)
        print("------------------------------------------------------------------")
        print("SUBJECT:", item.subject)
        print("BODY:")
        print(item.body)
        print("------------------------------------------------------------------")
        print(item)

server = os.environ['EXCHANGE_SERVER']
smtp_addr = os.environ['EMAIL']
username = smtp_addr
password = os.environ['PASSWORD']

config = Configuration(username=username, password=password)
log.debug("CONFIG COMPLETE ======================================\n\n\n\n\n\n")

account = Account(primary_smtp_address=smtp_addr,
                  config=config,
                  autodiscover=True,
                  access_type=DELEGATE)
log.debug("ACCOUNT SETUP COMPLETE ===============================\n\n\n\n\n\n")

print(account.protocol)

subscription = Subscription(folder=account.inbox,
                            events=[Subscription.CREATED_EVENT])

items = []

while True:
    events = subscription.get_events()
    for event, item in events:
        print_event(event, item)
        if item is not None:
            items.append(item)

    print()
    print("SubscriptionId", subscription.subscription.subscription_id)
    print("Watermark     ", subscription.subscription.watermark)
    print()
    print()
    cmd = input(("Enter when ready to continue\n"
                 "'R' to replay all received events\n"
                 "Anything else to quit\n"
                 ":"))
    print()

    if cmd == "R":
        if items:
            res = account.inbox.add_items(items)
            print(res)
            print("{} item(s) added successfully".format(len(items)))
        else:
            print("Nothing to Replay.")
    elif cmd:
        break

unsub = subscription.unsubscribe()
print("Successful unsubscribe. Goodbye.")
