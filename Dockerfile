FROM python:3.5

# RUN pip install exchangelib

RUN pip install requests
RUN pip install requests-ntlm
RUN pip install dnspython3
RUN pip install pytz
RUN pip install lxml

WORKDIR /usr/src

CMD bash
